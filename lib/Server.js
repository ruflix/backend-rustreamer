/**
 * TCD Software
 * Created by Dmitrij Rysanow on 21.12.16.
 */
"use strict";

const proxy = require('./Proxy'),
    bodyParser = require('body-parser'),
    serveStatic = require('serve-static'),
    log = require('npmlog'),
    feedParser = require('feed-read'),
    compression = require('compression'),
    RutrackerParser = require('rutracker-api'),
    Q = require('q'),
    TorrentAPI = require('./TorrentManagment/TorrentAPI'),
    dns = require('dns'),
    os = require('os'),
    request = require('request'),
    htmlToJson = require('html-to-json'),
    bytes = require('bytes');

const SERVER_NAME = "RuflixService";

class Server {
    constructor(conf) {
        require('events').EventEmitter.defaultMaxListeners = Infinity;
        try {
            this.Parser = new RutrackerParser();
            this.config = conf;
            this.torrentApi = new TorrentAPI(this.Parser);

            this.username = null;

        } catch (e) {
            log.error(e);
        }
    }

    static respondNotFound(res) {
        log.error(SERVER_NAME, "Not found");
        res.writeHead(404);
        res.write("Not found");
        res.end();
    }

    getHostnameAddress() {
        log.info(`descripting hostname`);
        let defer = Q.defer();
        if (this.config.hostname) {
            defer.resolve(this.config.hostname)
        } else {
            dns.lookup(os.hostname(), (err, add, fam) => {
                log.info(`hostname address ${add}`);
               defer.resolve(add);
            });
        }
        return defer.promise;
    }

    run() {
        let express = require('express');
        let defer = Q.defer();
        let app = express();

        app.use(compression());

        app.use(bodyParser.urlencoded({extended: false}));
        app.use(bodyParser.json());

        /* GET ui-rustreamer. */
        if (this.config.hostFront) {
            app.use('/', serveStatic(this.config.frontDir));
        }
        /* PROXY */
        app.use('/rutrackerapi', function (req, res, next) {
            console.log('Proxy Request to: '+ this.config.ApiEndpoint + req.url);
            next();
        }.bind(this));
        app.use('/rutrackerapi', proxy({ target: this.config.ApiEndpoint }));

        app.use('/feed',(req, res) => {
            feedParser(this.config.FeedEndpoint, onParsed);

            function onParsed(err, ret) {
                if (err) {
                    log.error(SERVER_NAME, "Cannot parse feed, " + err.message);
                } else {
                    log.info(SERVER_NAME,"Feed parsed. Response length " + ret.length);
                    res.end(JSON.stringify(ret));
                }
            }

        });

        app.use('/login', (req, res) => {
            if (req.method !== "POST") this.respondNotFound(res);
            log.info(SERVER_NAME, "Logging user " + req.body.username);
            this.Parser.login(req.body.username, req.body.password);
            this.Parser.on('login', () => {
                log.info(SERVER_NAME, 'Authorization successful');
                this.username = req.body.username;
                res.end(JSON.stringify({logged: true}));
            });
            this.Parser.on('error', (err) => {
                log.error(err);
                res.end(err);
            });
        });

        app.use('/checklogin', (req, res) => {
            if (this.Parser.cookie) {
                res.end(JSON.stringify({logged: true, username: this.username}));
            } else {
                res.end(JSON.stringify({logged: false}));
            }
        });
        
        app.use('/getconfig', (req, res) => {
            res.json(this.config);
        });

        app.use('/search', (req, res) => {
            if (req.method !== "POST") this.respondNotFound(res);
            log.info(SERVER_NAME, "Search " + req.body.query);
            this.Parser.search(req.body.query, (response) => {
                res.end(JSON.stringify(response));
            });
        });

        app.use('/getFiles/:torrentId', (req, res) => {
            let rutrackerId = req.params.torrentId;
            request.debug = true;
            if (!this.Parser.cookie) return res.status(401);
            let jar = request.jar();
            let cookie = request.cookie(this.Parser.cookie);
            let url = "http://rutracker.org/forum/viewtorrent.php";
            jar.setCookie(cookie, url);
            request({uri: url, jar: jar, method: 'POST', gzip: true, form:{t: rutrackerId}},
                (err, httpResponse, body) => {
                if (err) console.error(err);
                else {
                    let promise = htmlToJson.batch(body, {
                        files: htmlToJson.createParser(['.ftree li:not(.dir)', {
                            'name': ($file) => $file.find('b').text(),
                            'length': ($file) => bytes($file.find('i').text())
                        }])
                    });
                    promise.then(x => res.json(x));
                }
            });

        });

        /**
         * Connect torrent api
         */
        app.use('/torrentapi', this.torrentApi.Router());

        // catch 404 and forward to error handler
        app.use((req, res, next) => {
            var err = new Error('Not Found');
            err.status = 404;
            next(err);
        });

        app.use((req, res, next) => {
            if (req.app.get('env') === 'development') {
                log.info(SERVER_NAME, "Requested " + req.url);
                next();
            }
        });

        app.listen(this.config.port, this.config.hostname);
        defer.resolve()
        return defer.promise;
    }
}

module.exports = Server;