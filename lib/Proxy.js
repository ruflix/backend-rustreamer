/**
 * TCD Software
 * Created by Dmitrij Rysanow on 13.12.16.
 */
'use strict';

/**
 * ExpressJS route handler, that passes requests to proxy server.
 *
 * @param {object} Options for "http-proxy" module
 * @return {Function}
 */
module.exports = function (options) {
    'use strict';
    const request = require('request');

    //  return expressjs route handler
    return function (req, res) {
        var proxyConnectionConf = {
            uri: options.target + req.url,
            method: req.method,
            headers: {}
        };

        var pendingConnection = request(proxyConnectionConf);
        
        pendingConnection.on('response', function (response) {
            console.log('Proxy: Response from: '+ options.target + req.url + ' with status code '+ response.statusCode);
            if (response.statusCode === 200) {
                pendingConnection.pipe(res);
            } else {
                res.json({ error: 'Wrong response status code', message: 'Server responded width status code '+ response.statusCode });
            }
        });
        
        pendingConnection.on('error', function (error) {
            res.json({ error: error, message: 'Request error' });
        });
    };
};