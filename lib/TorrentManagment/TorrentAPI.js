/**
 * TCD Software
 * Created by Dmitrij Rysanow on 04.02.17.
 */
const WebTorrent = require('webtorrent'),
    log = require('npmlog'),
    express = require('express'),
    Q = require('q'),
    mime = require('mime'),
    clivas = require('clivas'),
    bytes = require('bytes'),
    rimraf = require('rimraf'),
    streamToArray = require('stream-to-array'),
    ffmpeg = require('fluent-ffmpeg');

const TERMINAL_UPDATE_INTERVAL = 200;
/**
 * Torrent managment class. Contains middleware (Router() method) for
 * communicating via HTTP Protocol
 *
 */
class TorrentAPI {

    /**
     *
     * @param {WebTorrent} Webtorrent Instance
     * @param {string|File} hash of Torrent or  Torrent File
     */
    constructor(rutrackerapi = null) {
        this.DOWNLOAD_CONFIG =
        {
            maxConns: 100        // Max number of connections per torrent (default=55)
        };
        /**
         *
         * @type {*} active torrent
         */
        this.activeTorrent = null;
        /**
         * defer awaiting download event
         * @type {Q.defer}
         */
        this.awaitingDownload = null;
        /**
         *
         * @type {stream} list of active torrents
         */
        this.server = null;
        /**
         * Actual stream info
         * @type {{stream: stream, files: Array<File>}}
         */
        this.streamInfo = null;
        /**
         * interval for updating Clivas console view
         * @type {int}
         */
        this.clivasInterval = null;

        this.rutrackerapi = rutrackerapi;

        process.on('SIGINT', this.onDestroy.bind(this));
    }

    /**
     * Creates stream server for torrent
     *
     *
     * @typedef {Object} StreamData
     * @param {Torrent} torrent
     * @return {{address: *, port: number}}
     */
    createServerFromTorrent(torrent) {
        if (this.server) {
            log.info('Destroying server');
            this.server.close();
        }

        let port = 9999;
        log.info('Starting stream server');
        this.server = torrent.createServer();
        this.server.on('error', this.onError);
        this.server.listen(port);

        return {
            "address": this.server.address().adress,
            "port": this.server.address().port
        };
    }

    onError(err) {
        log.error('TorrentManager', err);
        if (this.unsetClivas) {
            this.unsetClivas();
        }
    }

    setClivas() {
        function display() {
            /*jshint validthis:true */
            clivas.clear();
            clivas.line('{red: node-ruflix}');
            clivas.line('');
            this.activeTorrent && clivas.line('{yellow: Active torrent :} ' + this.activeTorrent.infoHash);
            this.server.address() && clivas.line('{yellow: Stream port :}' + this.server.address().port);
            this.activeTorrent && clivas.line('{yellow: Dowload speed :}' + bytes(this.activeTorrent.downloadSpeed) + '/s');
            this.activeTorrent && clivas.line('{yellow: Upload speed :}' + bytes(this.activeTorrent.uploadSpeed) + '/s');
            this.activeTorrent && clivas.line('{yellow: Downloaded :}' + bytes(this.activeTorrent.downloaded ));
            this.activeTorrent && clivas.line('{yellow: Peers :}' + this.activeTorrent.numPeers);
        }
        this.clivasInterval = setInterval(display.bind(this), TERMINAL_UPDATE_INTERVAL);
    }

    unsetClivas() {
        if (this.clivasInterval) {
            clearInterval(this.clivalInterval);
        }
    }

    getMetadata(req, res, next) {
        let index = req.params.index;

        ffmpeg.ffprobe(this.activeTorrent.files[index].createReadStream(),
            function (err, metadata) {
                log.error(err);
                res.json(metadata);
                next();
            });
    }

    onTorrent(torrent) {
        log.info("onTorrent");
        torrent.on('download', this.onDownload.bind(this));
        let defer = Q.defer();
        try {
            log.info(`torrent resolved, has ${torrent.files.length} files`);
            if (torrent) {
                torrent.resume();
                let stream = this.createServerFromTorrent(torrent);
                this.activeTorrent = torrent;
                let files = this.activeTorrent.files.map(function (file, index) {
                    return {
                        "id": index,
                        "name": file.path,
                        "length": file.length,
                        "done": file.done,
                        "mime": mime.lookup(file.path)
                    };
                });
                
                this.setClivas();

                this.streamInfo = {
                    files: files,
                    stream: stream
                };
                defer.resolve(this.streamInfo);
            } else {
                defer.reject("Not found");
            }
        } catch (e) {
            this.onError(e);
            defer.reject(e);
        }
        return defer.promise;
    }

    onDestroy() {
        this.unsetClivas();
        log.info("Caught interrupt signal");
        if (this.server) this.server.close();
        if (this.webtorrent) this.webtorrent.destroy();
        process.exit(0);
    }

    startTorrent() {
        if (!this.webtorrent) {
            /**
             * @type {WebTorrent} Webtorrent instance
             */
            this.webtorrent = new WebTorrent();
            this.webtorrent.on('error',this.onError);
        }
    }

    deleteTorrent(torrent) {
        log.info(`delete torrent ${torrent.infoHash}`);
        let defer = Q.defer();
        let path = torrent.path;
        function removePath() {
            rimraf(path,defer.resolve);
        }

        torrent.destroy(removePath.bind(this));
        return defer.promise;
    }

    onDownload(bytes) {
        if (this.awaitingDownload) {
            log.info("started download");
            this.awaitingDownload.resolve(this.streamInfo);
        }
        this.awaitingDownload = null;
    }

    /**
     * Saves torrent to this.torrents, returns it's id and it's files
     * @this
     * @param {string|Buffer} torrent identifier (hash/magnet/File)
     * @return {{id: {number}, array<number>}} List files
     */
    addTorrent(torrent) {
        log.info('add torrent');
        this.startTorrent();
        this.unsetClivas();
        let defer = Q.defer();
        let torrentCallback = function (torrent) {
            this.onTorrent(torrent);
        };
        if (this.activeTorrent && torrent == this.activeTorrent.infoHash) {
            //client attempting to add same torrent, it's all right, nothing to do.
            log.info("Attempt to add same torrent as active");
            defer.resolve.bind(this);
        } else if (this.webtorrent.get(torrent)) {
            //don't add duplicate torrent
            if (this.activeTorrent) {
                log.info(`Paused torrent ${this.activeTorrent.infoHash}`);
                log.info("Resume torrent");
                this.activeTorrent.pause();
            }
            this.activeTorrent = this.webtorrent.get(torrent);

            this.onTorrent(this.activeTorrent).then(defer.resolve.bind(this));
        } else if (this.activeTorrent) {
            //delete old and load new torrent
            this.unsetClivas();
            log.info(`Paused torrent ${this.activeTorrent.infoHash}`);
            log.info("Switch torrent");
            this.deleteTorrent(this.activeTorrent)
                .then(function () {
                    this.webtorrent.add(torrent, this.DOWNLOAD_CONFIG, torrentCallback.bind(this));
                }.bind(this));
            this.awaitingDownload = defer;
        } else {
            this.webtorrent.add(torrent, this.DOWNLOAD_CONFIG, torrentCallback.bind(this));
            this.awaitingDownload = defer;
        }

        return defer.promise;
    }

    add(req, res) {
        let hash = req.params.hash;
        log.info(`add torrent hash ${hash}`);
        this.addTorrent(hash)
            .then((streamInfo) => {
                res.setHeader('Cache-Control', 'no-store');
                res.json(streamInfo);
            }, (err) => {
                this.onError(err);
                res.writeHead(500);
                res.write(err);
                res.end();
            });
    }

    addFile(req, res) {
        let id = req.params.rutrackerTorrentId;
        log.info(`add torrent torrent id ${id}`);
        if (!this.rutrackerapi) {
            throw "rutrackerapi is not set";
        }
        try {
            this.rutrackerDownloadPromise(id)
                .then(this.onTorrentFileDownloaded.bind(this))
                .then((streamInfo) => {
                res.setHeader('Cache-Control', 'no-store');
                res.json(streamInfo);
            }, (err) => {
                this.onError(err);
                res.writeHead(500);
                res.write(err);
                res.end();
            });
        } catch (e) {
            this.onError(e);
        }
    }

    /**
     * @param {fs.ReadStream} response
     */
    onTorrentFileDownloaded(response) {

        log.info('onTorrentFileDownload');
        var self = this;
        return streamToArray(response)
            .then((parts) => {
                const buffers = parts
                    .map(part => Buffer.isBuffer(part) ? part : Buffer.from(part));
                return Buffer.concat(buffers);
            })
            .then(self.addTorrent.bind(this));
    }

    rutrackerDownloadPromise(id) {
        let defer = Q.defer();
        this.rutrackerapi.download(id, defer.resolve.bind(this));
        return defer.promise;
    }

    Router() {
        this.app = express();

        /**
         * Should add torrent to downloads
         * Should return object with torrent id, and list of files
         */
        this.app.use('/add/:hash', this.add.bind(this));
        this.app.use('/addFile/:rutrackerTorrentId', this.addFile.bind(this));
        this.app.use('/getMetadata/:index', this.getMetadata.bind(this));
        return this.app;
    }
}
module.exports = TorrentAPI;