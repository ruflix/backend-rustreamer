"use strict";

const argv = require('yargs').argv,
    extend = require('extend'),
    defaults = require('./config.json'),
    Server = require('./lib/Server'),
    log = require('npmlog');

var config = extend(defaults, argv);

function createServer(args) {
    if (args) {
        config = extend(defaults, args);
    }
    log.info("config file", config);
    var srv = new Server(config);
    return srv.run();
}

function getConfig() {
    return config;
}

if (config.run) {
    createServer(defaults);
}

module.exports = {
    run: createServer,
    getConfig: getConfig
};
