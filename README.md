**backend-rustreamer**

Node-Part of Ruflix project. 
It contains:

- Proxy server for http://rutracker.org and http://api.rutracker.org
- Reader of http://feed.rutracker.org
- WebTorrent part, provides methods to download and stream torrents

In future:

- File managment of streamed torrents
- Database, shared with ui
- Socket.io based communication with webtorrent and file manager
- I2P Proxy with hidden rutracker.org service


It's taking args, which will extend local config.json:

**config.hostFront** - boolean, if server will host static directory with html app. Default: true.

**config.frontDir** - string, directory of front to host. Default: _node_modules/ui-rustreamer_, which is devDependency.

**config.port** - int, determines port of host, Defaults: 7000

**config.ApiEndpoint** - string, URL of Rutracker API. Default: _http://api.rutracker.org:80_

**config.FeedEndpoint** - string, URL of Rutracker Feed. Default: _http://feed.rutracker.org/atom/f/1880.atom_

Methods

_'GET /rutrackerapi'_ - will be proxied to **config.RutrackerApiEndpoint**

_'GET /feed'_ - will request RSS Feed from **config.RutrackerFeedEndpoint**. 

Returns array of articles from feed.

_'POST /login' (String username, String password)_ - will login the user

Returns {logged: boolean} if user is successfully logged

_'GET /checklogin'_ - get authorization status

Returns {logged: boolean, username: ?string} if user is logged

_'POST /search'_ - (Only if logged) provides search of rutracker.org resource

Returns Array<{
      state    : string,
      id       : string,
      category : string,
      title    : string,
      author   : string,
      size     : string,
      seeds    : string,
      leechs   : string,
      url      : string
  }> with search results
  
_'GET /torrentapi/add/:hash'_ - downloads the torrent and creates stream

Returns {
files: Array<{
        name: string - file name,
        length: int - file size,
        done: boolean - if 100% of file is downloaded,
        mime: string - mime-type of file
    }> with files to stream,
stream: Array<{
        address: string - host of stream,
        port: int - port of stream
    }> - streaming